$(document).ready(function(){
    $("#productType").change(function(){
        var names = ["dvd.php", "book.php", "furniture.php"];
        var index = document.getElementById("productType").value;
        $(".remove").remove();
        if(index){
            $.ajax({
                url: "ajax/" + names[index],
                cache: false
            })
            .done(function(html) {
                $("#product_form").append(html);
            });
        }
    });

    $("#product_form").submit(function(event){
        event.preventDefault();
        var data = {};
        $(this).find(":input").each(function(){
            data[$(this).attr("name")] = $(this).val();
        });
        $.ajax({
            url: "/framework/add-product/add",
            type: "post",
            data: {
                data: data
            },
            success: function(response){
                if(response != ""){
                    var errors = JSON.parse(response);
                    $.each(errors, function(key, value){
                        $("#" + key).text(value);
                    });
                }else{
                    window.location.href = "/framework";
                }
            }
        });
    });
});
