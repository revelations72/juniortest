$(document).ready(function(){
    $("#delete-product-btn").click(function(){
        var skus = [];
        $(".card").find("input:checkbox:first").each(function(){
            if(this.checked){
                var sku = $(this).next().children().first().text();
                skus.push(sku);
            }
        });
        if(skus.length > 0){
            $.ajax({
                url: "/framework/productlist/delete",
                type: "post",
                data: {
                    skus: skus
                },
                success: function(response)
                {
                    location.reload();
                }
            });
        }
    });
});
