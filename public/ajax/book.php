<div class="row py-2 remove">
    <div class="col-2">
        <label for="weight" class="form-label">Weight (Kg)</label>
    </div>
    <div class="col-3">
        <input type="text" class="form-control" name="weight" id="weight">
    </div>
    <div class="col-3">
        <p class="error text-danger" id="error_weight"></p>
    </div>
    <p class="lead text-primary fw-bold pt-3">Please, provide weight</p>
</div>
