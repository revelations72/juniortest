<div class="row py-2 remove">
    <div class="col-2">
        <label for="height" class="form-label">Height (cm)</label>
    </div>
    <div class="col-3">
        <input type="text" class="form-control" name="height" id="height">
    </div>
    <div class="col-3">
        <p class="error text-danger" id="error_height"></p>
    </div>
</div>
<div class="row py-2 remove">
    <div class="col-2">
        <label for="width" class="form-label">Width (cm)</label>
    </div>
    <div class="col-3">
        <input type="text" class="form-control" name="width" id="width">
    </div>
    <div class="col-3">
        <p class="error text-danger" id="error_width"></p>
    </div>
</div>
<div class="row py-2 remove">
    <div class="col-2">
        <label for="length" class="form-label">Length (cm)</label>
    </div>
    <div class="col-3">
        <input type="text" class="form-control" name="length" id="length">
    </div>
    <div class="col-3">
        <p class="error text-danger" id="error_length"></p>
    </div>
    <p class="lead text-primary fw-bold pt-3">Please, provide dimensions</p>
</div>
