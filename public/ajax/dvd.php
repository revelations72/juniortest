<div class="row py-2 remove">
    <div class="col-2">
        <label for="size" class="form-label">Size (MB)</label>
    </div>
    <div class="col-3">
        <input type="text" class="form-control" name="size" id="size">
    </div>
    <div class="col-3">
        <p class="error text-danger" id="error_size"></p>
    </div>
    <p class="lead text-primary fw-bold pt-3">Please, provide size</p>
</div>
