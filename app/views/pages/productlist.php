<?php include APPROOT . "/views/templates/header.php"; ?>
    <script type="text/javascript" src="<?php echo URLROOT; ?>public/js/productlist.js"></script>
    <title>Product List</title>
</head>
<body style="padding-bottom: 90px;">
    <nav class="navbar bg-white">
        <div class="container-fluid mx-4 pt-4 pb-2 border-bottom border-dark">
            <h1 class="h1 ps-5">Product List</h1>
            <div class="navbar-buttons pe-5">
                <a href="add-product" id="add-product-btn" class="btn btn-primary">ADD</a>
                <button id="delete-product-btn" class="btn btn-danger">MASS DELETE</button>
            </div>
        </div>
    </nav>
<?php include APPROOT . "/views/posts/cards.php"; ?>
<?php include APPROOT . "/views/templates/footer.php"; ?>
