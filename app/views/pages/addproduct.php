<?php include APPROOT . "/views/templates/header.php"; ?>
    <script type="text/javascript" src="<?php echo URLROOT; ?>public/js/addproduct.js"></script>
    <title>Product Add</title>
</head>
<body>
    <nav class="navbar bg-white">
        <div class="container-fluid mx-4 pt-4 pb-2 border-bottom border-dark">
            <h1 class="h1 ps-5">Product Add</h1>
            <div class="navbar-buttons pe-5">
                <button type="submit" form="product_form" class="btn btn-primary">Save</button>
                <a href="../" class="btn btn-danger">Cancel</a>
            </div>
        </div>
    </nav>
<?php include APPROOT . "/views/posts/form.php"; ?>
<?php include APPROOT . "/views/templates/footer.php"; ?>
