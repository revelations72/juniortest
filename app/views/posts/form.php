<form class="form px-5" id="product_form" method="post">
    <div class="row py-2">
        <div class="col-2">
            <label for="sku" class="form-label">SKU</label>
        </div>
        <div class="col-3">
            <input type="text" class="form-control" name="sku" id="sku">
        </div>
        <div class="col-3">
            <p class="error text-danger" id="error_sku"></p>
        </div>
    </div>
    <div class="row py-2">
        <div class="col-2">
            <label for="name" class="form-label">Name</label>
        </div>
        <div class="col-3">
            <input type="text" class="form-control" name="name" id="name">
        </div>
        <div class="col-3">
            <p class="error text-danger" id="error_name"></p>
        </div>
    </div>
    <div class="row py-2">
        <div class="col-2">
            <label for="price" class="form-label">Price ($)</label>
        </div>
        <div class="col-3">
            <input type="text" class="form-control" name="price" id="price">
        </div>
        <div class="col-3">
            <p class="error text-danger" id="error_price"></p>
        </div>
    </div>
    <div class="row py-2">
        <div class="col-2">
            <label for="productType" class="form-label">Type Switcher</label>
        </div>
        <div class="col-3">
            <select class="form-select" name="productType" id="productType">
                <option selected value="">-</option>
                <option value="0">DVD</option>
                <option value="1">Book</option>
                <option value="2">Furniture</option>
            </select>
        </div>
        <div class="col-3">
            <p class="error text-danger" id="error_productType"></p>
        </div>
    </div>
</form>
