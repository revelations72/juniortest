<div class="container">
    <div class="row">
        <?php foreach ($data as $product) { ?>
            <div class="col-4">
                <div class="card bg-light m-3">
                    <input class="delete-checkbox mx-3 mt-3" type="checkbox">
                    <div class="card-body text-center">
                        <p class="card-text"><?php echo $product->sku; ?></p>
                        <p class="card-text"><?php echo $product->name; ?></p>
                        <p class="card-text"><?php echo $product->price; ?> $</p>
                        <p class="card-text"><?php echo $product->attribute; ?></p>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
</div>
