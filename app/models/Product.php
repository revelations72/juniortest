<?php

namespace App\Models;

use App\Libraries\Database;

class Product
{
    private $database;

    public function __construct()
    {
        $this->database = new Database();
    }

    public function getData()
    {
        $this->database->query("SELECT * FROM products");
        return $this->database->result();
    }

    public function addData($data)
    {
        $this->database->query("INSERT INTO products (sku, name, price, productType, attribute)
        VALUES (:sku, :name, :price, :productType, :attribute)");
        $this->database->bind(":sku", $data["sku"]);
        $this->database->bind(":name", $data["name"]);
        $this->database->bind(":price", $data["price"]);
        $this->database->bind(":productType", $data["productType"]);
        $this->database->bind(":attribute", $data["attribute"]);
        $this->database->execute();
    }

    public function deleteData($data)
    {
        foreach ($data as $sku) {
            $this->database->query("DELETE FROM products WHERE sku = :sku");
            $this->database->bind(":sku", $sku);
            $this->database->execute();
        }
    }
}
