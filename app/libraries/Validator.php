<?php

namespace App\Libraries;

class Validator
{
    private $data;
    private $newData;

    public function __construct($data)
    {
        $this->data = $data;
        $type = ["DVD", "Book", "Furniture"];
        if (isset($data["productType"]) && strlen($this->data["productType"]) > 0) {
            $index = $data["productType"];
        } else {
            $index = rand(0, 2);
        }
        require_once "../app/controllers/Product.php";
        require_once "../app/controllers/" . $type[$index] . ".php";
        $class = "App\Controllers\\" . $type[$index];
        $this->validate(new $class($data));
    }

    public function validate($product)
    {
        $product->validateSKU();
        $product->validateName();
        $product->validatePrice();
        $product->validateProductType();
        $product->validateAttribute();

        if (!array_filter($product->getErrors())) {
            $this->newData = $product->getData();
            $this->sendData();
        } else {
            echo json_encode($product->getErrors());
        }
    }

    public function sendData()
    {
        return $this->newData;
    }
}
