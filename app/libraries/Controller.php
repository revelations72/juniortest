<?php

namespace App\Libraries;

abstract class Controller
{
    public function model($model)
    {
        require_once "../app/models/" . $model . ".php";
        $model = "App\Models\\" . $model;
        return new $model();
    }

    public function view($view, $data = [])
    {
        $file = "../app/views/" . $view . ".php";
        if (file_exists($file)) {
            require_once $file;
        } else {
            die("View does not exist");
        }
    }
}
