<?php

namespace App\Libraries;

class Core
{
    private $controller = "ProductList";
    private $method = "index";
    private $parameters = [];

    public function __construct()
    {
        $url = $this->getURL() ? $this->getURL() : [0];

        if ($url[0] == "add-product") {
            if (file_exists("../app/controllers/AddProduct.php")) {
                $this->controller = "AddProduct";
                unset($url[0]);
            }
        } else {
            if (file_exists("../app/controllers/" . ucwords($url[0]) . ".php")) {
                $this->controller = ucwords($url[0]);
                unset($url[0]);
            }
        }

        require_once "../app/controllers/" . $this->controller . ".php";
        $this->controller = "App\Controllers\\" . $this->controller;
        $this->controller = new $this->controller();

        if (isset($url[1])) {
            if (method_exists($this->controller, $url[1])) {
                $this->method = $url[1];
                unset($url[1]);
            }
        }

        $this->parameters = $url ? array_values($url) : [];

        call_user_func_array([$this->controller, $this->method], $this->parameters);
    }

    public function getURL()
    {
        if (isset($_GET["url"])) {
            $url = rtrim($_GET['url'], '/');
            $url = filter_var($url, FILTER_SANITIZE_URL);
            $url = explode('/', $url);
            return $url;
        }
    }
}
