<?php

spl_autoload_register(function ($class) {
    $prefix = "App\\Libraries\\";
    $directory = APPROOT . "/libraries/";
    $name = substr_replace($class, "", 0, strlen($prefix));
    $file = $directory . $name . ".php";
    if (file_exists($file)) {
        require $file;
    }
});
