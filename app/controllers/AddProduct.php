<?php

namespace App\Controllers;

use App\Libraries\Controller;
use App\Libraries\Validator;

class AddProduct extends Controller
{
    public function index()
    {
        $this->view("pages/addproduct");
    }

    public function add()
    {
        $data = $_POST["data"];
        $validator = new Validator($data);
        if ($validator->sendData() != "") {
            $this->model("Product")->addData($validator->sendData());
        }
    }
}
