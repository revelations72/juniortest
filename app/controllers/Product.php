<?php

namespace App\Controllers;

abstract class Product
{
    protected $data;

    protected $sku;
    protected $name;
    protected $price;
    protected $productType;
    protected $attribute;

    protected $array;
    protected $errors;
    protected $messages = ["Please, submit required data", "Please, provide the data of indicated type"];

    abstract protected function validateAttribute();

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function validateSKU()
    {
        if ($this->data["sku"] == " " || strlen($this->data["sku"]) == 0) {
            $this->errors["error_sku"] = $this->messages[0];
        } else {
            $this->sku = $this->data["sku"];
            $this->errors["error_sku"] = "";
        }
    }

    public function validateName()
    {
        if ($this->data["name"] == " " || strlen($this->data["name"]) == 0) {
            $this->errors["error_name"] = $this->messages[0];
        } else {
            $this->name = $this->data["name"];
            $this->errors["error_name"] = "";
        }
    }

    public function validatePrice()
    {
        if ($this->data["price"] == " " || strlen($this->data["price"]) == 0) {
            $this->errors["error_price"] = $this->messages[0];
        } elseif (!preg_match("/^[0-9]+$/", $this->data["price"])) {
            $this->errors["error_price"] = $this->messages[1];
        } else {
            $this->price = $this->data["price"];
            $this->errors["error_price"] = "";
        }
    }

    public function validateProductType()
    {
        if ($this->data["productType"] == " " || strlen($this->data["productType"]) == 0) {
            $this->errors["error_productType"] = $this->messages[0];
        } else {
            $this->productType = $this->data["productType"];
            $this->errors["error_productType"] = "";
        }
    }

    public function getErrors()
    {
        return $this->errors;
    }

    public function getData()
    {
        $this->array["sku"] = $this->sku;
        $this->array["name"] = $this->name;
        $this->array["price"] = $this->price;
        $this->array["productType"] = $this->productType;
        $this->array["attribute"] = $this->attribute;
        return $this->array;
    }
}
