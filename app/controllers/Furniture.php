<?php

namespace App\Controllers;

class Furniture extends Product
{
    private $height;
    private $width;
    private $length;

    public function validateAttribute()
    {
        if (isset($this->data["height"]) && isset($this->data["width"]) && isset($this->data["length"])) {
            if ($this->data["height"] == " " || strlen($this->data["height"]) == 0) {
                $this->errors["error_height"] = $this->messages[0];
            } elseif (!preg_match("/^[0-9]+$/", $this->data["height"])) {
                $this->errors["error_height"] = $this->messages[1];
            } else {
                $this->height = $this->data["height"];
                $this->errors["error_height"] = "";
            }

            if ($this->data["width"] == " " || strlen($this->data["width"]) == 0) {
                $this->errors["error_width"] = $this->messages[0];
            } elseif (!preg_match("/^[0-9]+$/", $this->data["width"])) {
                $this->errors["error_width"] = $this->messages[1];
            } else {
                $this->width = $this->data["width"];
                $this->errors["error_width"] = "";
            }

            if ($this->data["length"] == " " || strlen($this->data["length"]) == 0) {
                $this->errors["error_length"] = $this->messages[0];
            } elseif (!preg_match("/^[0-9]+$/", $this->data["length"])) {
                $this->errors["error_length"] = $this->messages[1];
            } else {
                $this->length = $this->data["length"];
                $this->errors["error_length"] = "";
            }

            $this->attribute = $this->height . "x" . $this->width . "x" . $this->length;
        }
    }
}
