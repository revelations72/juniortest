<?php

namespace App\Controllers;

class DVD extends Product
{
    public function validateAttribute()
    {
        if (isset($this->data["size"])) {
            if ($this->data["size"] == " " || strlen($this->data["size"]) == 0) {
                $this->errors["error_size"] = $this->messages[0];
            } elseif (!preg_match("/^[0-9]+$/", $this->data["size"])) {
                $this->errors["error_size"] = $this->messages[1];
            } else {
                $this->attribute = $this->data["size"] . " MB";
                $this->errors["error_size"] = "";
            }
        }
    }
}
