<?php

namespace App\Controllers;

use App\Libraries\Controller;

class ProductList extends Controller
{
    public function index()
    {
        $data = $this->model("Product")->getData();
        $this->view("pages/productlist", $data);
    }

    public function delete()
    {
        $data = $_POST["skus"];
        $this->model("Product")->deleteData($data);
    }
}
