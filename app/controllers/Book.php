<?php

namespace App\Controllers;

class Book extends Product
{
    public function validateAttribute()
    {
        if (isset($this->data["weight"])) {
            if ($this->data["weight"] == " " || strlen($this->data["weight"]) == 0) {
                $this->errors["error_weight"] = $this->messages[0];
            } elseif (!preg_match("/^[0-9]+$/", $this->data["weight"])) {
                $this->errors["error_weight"] = $this->messages[1];
            } else {
                $this->attribute = $this->data["weight"] . " Kg";
                $this->errors["error_weight"] = "";
            }
        }
    }
}
